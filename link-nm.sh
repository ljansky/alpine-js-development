#!/bin/bash

[ -h /usr/src/out/$1/node_modules ] && unlink /usr/src/out/$1/node_modules
ln -s /usr/src/nm/$1/node_modules /usr/src/out/$1/node_modules

for d in `find . -maxdepth 1 -mindepth 1 -type d`; do
	if [ "$d" != $1 ] ; then 
		[ -h /usr/src/out/$d/node_modules ] && unlink /usr/src/out/$d/node_modules
		ln -s /usr/src/nm/$d/node_modules /usr/src/out/$d/node_modules

		[ -h /usr/src/out/$1/node_modules/$d ] && unlink /usr/src/out/$1/node_modules/$d
		ln -s /usr/src/out/$d /usr/src/out/$1/node_modules/$d
	fi
done

if [ $2 == "build" ] ; then
	cd /usr/src/out && lerna run --scope $1 --include-filtered-dependencies $1 $2 && lerna run --scope $1 $1 $2
fi

if [ $2 == "dev" ] ; then
	cd /usr/src/out && lerna run --parallel --scope $1 --include-filtered-dependencies $1 $2
fi

if [ $2 == "start" ] ; then
	cd /usr/src/out && lerna run --scope $1 --include-filtered-dependencies $1 build
	cd /usr/src/out/$1 && pm2-docker process.json
fi