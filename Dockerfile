FROM node:alpine

RUN apk add --update bash git && rm -f /var/cache/apk/*

RUN npm install pm2 webpack babel-cli lerna -g

COPY lerna.json /usr/src/nm/lerna.json
COPY package.json /usr/src/nm/package.json

COPY lerna.json /usr/src/out/lerna.json
COPY package.json /usr/src/out/package.json

COPY link-nm.sh /usr/local/bin/link-nm.sh
RUN chmod 777 /usr/local/bin/link-nm.sh

COPY install-nm.sh /usr/local/bin/install-nm.sh
RUN chmod 777 /usr/local/bin/install-nm.sh

WORKDIR /usr/src/out

ENTRYPOINT [ "link-nm.sh" ]